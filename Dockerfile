FROM golang:1.11.1

ADD hello.go hello.go

EXPOSE 8080

CMD ["go", "run", "hello.go"]